-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify2
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown x = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const x) = x
eval (Mult x y) = eval x * eval y
eval (Add x y) = eval x + eval y
eval (Sub x y) = eval x - eval y

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Mult x y) = simplify2 (simplify2 (Mult x y))
simplify x = simplify2 x

simplify2 :: Term -> Term
simplify2 (Const x) = Const x

simplify2 (Mult x (Add n m)) = Add (simplify2 (Mult (simplify2 x) (simplify2 n))) (simplify2 (Mult (simplify2 x) (simplify2 m)))
simplify2 (Mult (Add n m) x) = Add (simplify2 (Mult (simplify2 n) (simplify2 x))) (simplify2 (Mult (simplify2 m) (simplify2 x)))
simplify2 (Mult x (Sub n m)) = Sub (simplify2 (Mult (simplify2 x) (simplify2 n))) (simplify2 (Mult (simplify2 x) (simplify2 m)))
simplify2 (Mult (Sub n m) x) = Sub (simplify2 (Mult (simplify2 n) (simplify2 x))) (simplify2 (Mult (simplify2 m) (simplify2 x)))
 
simplify2 (Mult x y) = Mult (simplify2 x) (simplify2 y)
simplify2 (Add x y) = Add (simplify2 x) (simplify2 y)
simplify2 (Sub x y) = Sub (simplify2 x) (simplify2 y)



showTerm :: Term -> String
showTerm (Const x) = show x
showTerm (Add x y) = "(" ++ (showTerm x) ++ " + " ++ (showTerm y) ++ ")"
showTerm (Mult x y) = "(" ++ (showTerm x) ++ " * " ++ (showTerm y) ++ ")"
showTerm (Sub x y) = "(" ++ (showTerm x) ++ " - " ++ (showTerm y) ++ ")"
