import Data.List

data Term = Var String
          | App Term Term
          | Lam String Term
          deriving (Show, Eq)

freeVars :: Term -> [String]
freeVars (Var s) = [s]
freeVars (App a b) = freeVars a `union` freeVars b
freeVars (Lam c d) = freeVars d \\ [c]


boundVars :: Term -> [String]
boundVars (Var _) = []
boundVars (App term1 term2) = (boundVars term1) `union` (boundVars term2)
boundVars (Lam name term) = name : (boundVars term)


replace :: String -> Term -> Term -> Term
replace l t (Var b) = if b == l then t else Var b
replace l t (App term1 term2) = App (replace l t term1) (replace l t term2)
replace l t (Lam name term) | name == l = Lam name term
                            | name `elem` (freeVars t) || name `elem` (boundVars t)= 
                              Lam (replace' t term name) (replace l t (replace name (Var (replace' t term name)) term))
                            | otherwise = Lam name (replace l t term)


replace' :: Term -> Term -> String -> String
replace' t y x = if x `elem` (freeVars t ++ boundVars t) then replace' t y (x ++ "'") else x

eval :: Term -> Term
eval (Lam n b) = (Lam n b)
eval t = eval' t []

eval' :: Term -> [Term] -> Term
eval' (App f a) as = eval' f (a:as)
eval' (Lam n b) [] = Lam n (eval b)
eval' (Lam n b) (a:as) = eval' (replace n a b) as
eval' f as = foldl App f (map eval as)
